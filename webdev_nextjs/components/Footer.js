import Heading from "./Heading";

function Footer(props) {
    return (
        <footer>

            <Heading text="Footer" tag="h3"/>
        </footer>
    );
}

export default Footer;