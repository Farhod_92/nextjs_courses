import React from 'react';
import Heading from "./Heading";

function ContactInfo({contact}) {
    const {name, email, address} = contact || {}
    const {street, suite, city, zipcode } = address

    if(!contact){
        return <Heading tag="h1" text={"Empty contact"}/>
    }

    return (
        <div>
            <Heading tag="h3" text={name} />
            <div>
                <strong>Email:</strong>{email}
            </div>
            <div>
                <strong>Address:</strong>{`${street}, ${suite}, ${city}, ${zipcode}`}
            </div>
        </div>

    );
}

export default ContactInfo;