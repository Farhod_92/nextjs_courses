import React from 'react';
import Header from "./Header";
import Footer from "./Footer";
import Head from "next/head";

function Layout({children, title}) {
    const TITLE = title || "HOME"
    return (
        <div id="LAYOUT">
            <Head>
                <title>{TITLE}</title>
                <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300&display=swap" rel="stylesheet"/>
            </Head>
            <Header/>
            {children}
            <Footer/>
        </div>
    );
}

export default Layout;