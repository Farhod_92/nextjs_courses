import React from 'react';
import Heading from "./Heading";

function PostInfo({post}) {
    const {userId, id, title, body} = post || {}

    if(!post){
        return <Heading tag="h1" text={"Empty post"}/>
    }

    return (
        <div>
            <Heading tag="h3" text={title} />
            <div>
                <strong>Body:</strong>{body}
            </div>
            <div>
                <strong>User:</strong>{`${userId}`}
            </div>
        </div>

    );
}

export default PostInfo;
