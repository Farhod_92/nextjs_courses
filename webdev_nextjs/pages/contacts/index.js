import React, {useEffect, useState} from 'react';
import Heading from "../../components/Heading";
import Link from "next/link";

function Contacts({contacts}) {

    return (
        <div>
            <Heading text={"Contacts List:"}></Heading>
            <ul>
                {contacts && contacts.map(({id, name})=>(
                    <li key={id}>
                        <Link href={`contacts/${id}`}>{name}</Link>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default Contacts;

export async function getStaticProps() {
    const  data = await fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())

    if(!data){
        return {
            notFound: true
        }
    }

    return {
        props: {contacts:data}, // will be passed to the page component as props
    }
}
