import React from 'react';
import Head from "next/head";
import ContactInfo from "../../components/ContactInfo";

function Id({contact}) {
    return (
        <div>
           <Head>
               <title>Contact</title>
           </Head>
            <ContactInfo contact={contact} />
        </div>
    );
}

export default Id;
export async function getServerSideProps({query}) {
    console.log(query)
    const  data = await fetch('https://jsonplaceholder.typicode.com/users/'+query.id)
        .then(response => response.json())


    if(!data){
        return {
            notFound: true
        }
    }
    return {
        props: {contact:data}, // will be passed to the page component as props
    }
}
