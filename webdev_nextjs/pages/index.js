import Heading from "../components/Heading";
import styles from "../styles/Home.module.scss"
import Socials from "../components/Socials";

const Home = ({socials}) =>(
    <div className={styles.wrapper}>
      <Heading text={"HEllo !!"}></Heading>
        <Socials socials={socials}/>
    </div>
)
 export default Home

export async function getStaticProps() {
    const  data = await fetch('http://localhost:3000/api/socials')
        .then(response => response.json())

    if(!data){
        return {
            notFound: true
        }
    }

    return {
        props: {socials:data}, // will be passed to the page component as props
    }
}
