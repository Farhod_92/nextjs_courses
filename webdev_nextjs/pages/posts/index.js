import React, {useEffect, useState} from 'react';
import Heading from "../../components/Heading";
import Link from "next/link";

function Posts({posts}) {

    return (
        <div>
            <Heading text={"Posts:"}></Heading>
            <ul>
                {posts && posts.map(({id, title})=>(
                    <li key={id}>
                        <Link href={`posts/${id}`}>{title}</Link>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default Posts;

export async function getStaticProps() {
    const  data = await fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())

    if(!data){
        return {
            notFound: true
        }
    }

    return {
        props: {posts:data}, // will be passed to the page component as props
    }
}
