import React from 'react';
import Head from "next/head";
import ContactInfo from "../../components/ContactInfo";
import PostInfo from "../../components/PostInfo";

function Post({post}) {
    return (
        <div>
           <Head>
               <title>Post</title>
           </Head>
            <PostInfo post={post} />
        </div>
    );
}

export default Post;

export async function getServerSideProps({query}) {
    console.log(query)
    const  data = await fetch('https://jsonplaceholder.typicode.com/posts/'+query.id)
        .then(response => response.json())


    if(!data){
        return {
            notFound: true
        }
    }
    return {
        props: {post:data}, // will be passed to the page component as props
    }
}
