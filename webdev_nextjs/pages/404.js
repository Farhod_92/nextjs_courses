import {useEffect} from "react"
import {useRouter} from "next/router";
import Heading from "../components/Heading";

function Error(props) {
    const router = useRouter();

    useEffect(()=>{
        setTimeout(()=>{router.push("/")}, 3000)
    }, [router])

    return (
        <div>
            <Heading text={"404"}></Heading>
            <Heading text="page not found......." tag="h2"></Heading>
        </div>
    );
}

export default Error;