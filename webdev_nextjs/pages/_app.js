import '../styles/globals.scss'
import Layout from "../components/Layout";
import youtuberImg from "../public/youtube.png"
import Image from "next/image";
import {useRouter} from "next/router";

const MyApp = ({ Component, pageProps }) => {
    const {pathname} = useRouter();
    const title = pathname.replace('/','')
   // title=title.toUpperCase();

  return (
  <Layout >
    <main>
      <Component {...pageProps} />
    </main>
    <Image
        src={youtuberImg}
        width={500}
        height={350}
        alt="preview"
        placeholder="blur"
    />
  </Layout>)
}

export default MyApp

// https://www.youtube.com/watch?v=ZCCEl9J8zv0&list=PLNkWIWHIRwMHjz7hM5o10BNc6dq0OMd2U&index=8
